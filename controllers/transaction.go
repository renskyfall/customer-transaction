package controllers

import (
	"fmt"
	"net/http"
	"sort"
	"testGin/models"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)
type ResponsePoin struct{
    AccountId   uint
    Name        string
    Point       int64
}

type InputPembukuan struct{
    AccountId    uint
    StartDate    string
    EndDate      string
}

type ResponseBuku struct{
    TransactionDate      string          `json:"transaction_date"`
    Description          string          `json:"description"`
    Debit                string          `json:"debit"`
    Credit               string          `json:"credit"`
    Balance              float64         `json:"balance"`
}

type CreateTransactionInput struct{
    AccountId            uint            `json:"account_id"`
    TransactionDate      string          `json:"transaction_date"`
    Description          string          `json:"description"`
    DebitCredit          string          `json:"debit_credit"`
    Amount               float64         `json:"amount"`
}

//API POST
func CreateTransaction(c *gin.Context) {
    // Validate input
    var input CreateTransactionInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    date := "2006-01-02"
    newDate, _ := time.Parse(date, input.TransactionDate)

    // Create transaction
    transaction := models.Transaction{AccountId: input.AccountId, TransactionDate: newDate, Description: input.Description, DebitCredit: input.DebitCredit, Amount: input.Amount}

    customer := findCustomerById(c, transaction.AccountId)
    newCustomer := models.Customer{ID: customer.ID, Name: customer.Name, Balance: customer.Balance}
    db := c.MustGet("db").(*gorm.DB)

    if transaction.DebitCredit == "C" {
        newBalance := transaction.Amount
        newCustomer.Balance += newBalance
        db.Model(&customer).Updates(newCustomer)
        db.Create(&transaction)
    } else if transaction.DebitCredit == "D"{
        newBalance := transaction.Amount
        if transaction.Amount > customer.Balance{
            c.JSON(http.StatusBadRequest, gin.H{"error": "Saldo anda tidak cukup"})
            return
        }else{
            newCustomer.Balance -= newBalance
            db.Model(&customer).Updates(newCustomer)
            db.Create(&transaction)
        }
    }

    c.JSON(http.StatusOK, gin.H{"data": transaction})
}



func findAllCustomer(c *gin.Context) (list []models.Customer) {
    db := c.MustGet("db").(*gorm.DB)
    var customerList []models.Customer
    db.Find(&customerList)
    return customerList
}

func findCustomerById(c *gin.Context, id uint)(newCustomer models.Customer) { 
    var customer models.Customer

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", id).First(&customer).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    return customer
}

//API Total Point
func TotalPoint(c *gin.Context) { 
    var result []ResponsePoin
    customerList := findAllCustomer(c)
    
    for _, customer := range customerList {
        transactionList := findTransactionByCustomerId(c,customer.ID)
        var tempResponsePoin ResponsePoin
        var poinSaya int64
        
        for _, transaction := range transactionList {
            
            if transaction.Description == "Beli Pulsa" {
                poinSaya += beliPulsaPoint(transaction.Amount)
            } else if transaction.Description == "Bayar Listrik"{
                poinSaya += bayarListrikPoint(transaction.Amount)
            } else {
                poinSaya += 0
            }
        }

        tempResponsePoin.AccountId = customer.ID
        tempResponsePoin.Name = customer.Name
        tempResponsePoin.Point = poinSaya
        result = append(result, tempResponsePoin)
    }

    c.JSON(http.StatusOK, gin.H{"data": result})
}

type timeSlice []models.Transaction

func (p timeSlice) Len() int {
    return len(p)
}

func (p timeSlice) Less(i, j int) bool {
    return p[i].TransactionDate.Before(p[j].TransactionDate)
}

func (p timeSlice) Swap(i, j int) {
    p[i], p[j] = p[j], p[i]
}

//API Pembukuan
func Pembukuan(c *gin.Context) { 
    
    var input InputPembukuan
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    transactionList := findTransactionByCustomerId(c, input.AccountId)
    
    date_sorted_reviews := make(timeSlice, 0, len(transactionList))

    for _, d := range transactionList {
        date_sorted_reviews = append(date_sorted_reviews, d)
    }
    sort.Sort(date_sorted_reviews)
    var listResult []ResponseBuku
    var TotalBalance float64
    for _, data := range transactionList {
        var buku ResponseBuku
        buku.TransactionDate = data.TransactionDate.Format("2006/01/02")
        buku.Description = data.Description
        if data.DebitCredit == "C"{
            buku.Credit = fmt.Sprint(data.Amount)
            buku.Debit = "-"
            TotalBalance += data.Amount
        }else{
            buku.Credit = "-"
            buku.Debit = fmt.Sprint(data.Amount)
            TotalBalance -= data.Amount
        }
        
        buku.Balance = TotalBalance
        listResult = append(listResult, buku)
    }


    
    format := "2006/01/02"
    StartDate, _ := time.Parse(format, input.StartDate)
    EndDate, _ := time.Parse(format, input.EndDate)
    var finalResult []ResponseBuku
    for _, data := range listResult {
        dataDate, _ := time.Parse(format, data.TransactionDate)
        if (dataDate.Equal(StartDate) || dataDate.After(StartDate)) && (dataDate.Equal(EndDate) || dataDate.Before(EndDate)){
            finalResult = append(finalResult, data)
        }
        

    }

    c.JSON(http.StatusOK, gin.H{"data": finalResult})
}

func findTransactionByCustomerId(c *gin.Context, id uint)(listTransaction []models.Transaction) { 
    var list []models.Transaction

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("account_id = ?", id).Find(&list).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    return list
}

func beliPulsaPoint(amount float64) (poin int64){
    var hasil int64

    if amount <= 10000{
        hasil = 0
    } else if amount <= 30000{
        temp := (amount - 10000) / 1000
        hasil = int64(temp * 1)
    } else {
        temp1 := 20000 / 1000
        temp2 := (amount - 30000) / 1000
        result1 := int64(temp1 * 1)
        result2 := int64(temp2 * 2)
        hasil = result1 + result2
    }
    // if amount > 30000 {
    //     temp := amount / 1000
    //     hasil = int64(temp * 2) 
    // } else if amount > 10000 {
    //     temp := amount / 1000
    //     hasil = int64(temp * 1)
    // } else{
    //     hasil = 0
    // }

    return hasil
}

func bayarListrikPoint(amount float64) (poin int64){
    var hasil int64

    if amount <= 50000{
        hasil = 0
    } else if amount <= 100000{
        temp := (amount - 50000) / 2000
        hasil = int64(temp * 1)
    } else {
        temp1 := 50000 / 2000
        temp2 := (amount - 100000) / 2000
        result1 := int64(temp1 * 1)
        result2 := int64(temp2 * 2)
        hasil = result1 + result2
    }
    // if amount > 100000 {
    //     temp := amount / 2000
    //     hasil = int64(temp * 2) 
    // } else if amount > 50000 {
    //     temp := amount / 2000
    //     hasil = int64(temp * 1) 
    // } else{
    //     hasil = 0
    // }

    return hasil
}