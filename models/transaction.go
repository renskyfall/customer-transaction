package models

import (
    "time"
)

type Transaction struct {
    ID                   uint            `json:"id" gorm:"primary_key"`
    AccountId            uint            `json:"account_id"`
    TransactionDate      time.Time       `json:"transaction_date"`
    Description          string          `json:"description"`
    DebitCredit          string          `json:"debit_credit"`
    Amount               float64         `json:"amount"`
}