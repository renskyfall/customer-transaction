package models


type Customer struct {
    ID          uint      `json:"id" gorm:"primary_key"`
    Name        string    `json:"userName"`
    Balance     float64   `json:"balance"`
}