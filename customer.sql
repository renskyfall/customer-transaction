/*
 Navicat Premium Data Transfer

 Source Server         : renod-mysql
 Source Server Type    : MySQL
 Source Server Version : 100316
 Source Host           : localhost:3306
 Source Schema         : customer

 Target Server Type    : MySQL
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 03/09/2021 16:19:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `balance` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES (1, 'Reno', NULL);
INSERT INTO `customers` VALUES (2, 'Dimas', NULL);
INSERT INTO `customers` VALUES (3, 'Koume', NULL);

-- ----------------------------
-- Table structure for transactions
-- ----------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `transaction_date` datetime(0) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `debit_credit` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `amount` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transactions
-- ----------------------------
INSERT INTO `transactions` VALUES (1, 1, '2021-09-08 14:40:55', 'Bayar Listrik', 'D', 70000);
INSERT INTO `transactions` VALUES (2, 1, '2021-09-15 14:40:59', 'Beli Pulsa', 'C', 60000);
INSERT INTO `transactions` VALUES (3, 1, '2021-09-21 14:41:03', 'Beli Pulsa', 'D', 5000);
INSERT INTO `transactions` VALUES (4, 2, '2021-09-21 13:44:32', 'Beli Pulsa', 'D', 41000);
INSERT INTO `transactions` VALUES (5, 2, '2021-09-22 13:44:35', 'Bayar Listrik', 'D', 20000);
INSERT INTO `transactions` VALUES (6, 1, '2021-09-02 07:00:00', 'Bayar Listrik', 'C', 5000);
INSERT INTO `transactions` VALUES (7, 2, '2021-09-02 07:00:00', 'Bayar Listrik', 'C', 5000);
INSERT INTO `transactions` VALUES (8, 3, '2017-01-01 07:00:00', 'Setor Tunai', 'C', 200000);
INSERT INTO `transactions` VALUES (9, 3, '2017-01-05 07:00:00', 'Beli Pulsa', 'D', 10000);
INSERT INTO `transactions` VALUES (10, 3, '2017-01-06 07:00:00', 'Bayar Listrik', 'D', 70000);
INSERT INTO `transactions` VALUES (11, 3, '2017-01-07 07:00:00', 'Tarik Tunai', 'D', 100000);
INSERT INTO `transactions` VALUES (12, 3, '2017-02-01 07:00:00', 'Setor Tunai', 'C', 300000);
INSERT INTO `transactions` VALUES (13, 3, '2017-02-05 07:00:00', 'Bayar Listrik', 'D', 50000);
INSERT INTO `transactions` VALUES (14, 3, '2017-02-15 07:00:00', 'Tarik Tunai', 'D', 50000);
INSERT INTO `transactions` VALUES (15, 3, '2017-02-20 07:00:00', 'Beli Pulsa', 'D', 40000);
INSERT INTO `transactions` VALUES (16, 3, '2017-02-28 07:00:00', 'Tarik Tunai', 'D', 50000);
INSERT INTO `transactions` VALUES (17, 3, '2017-03-01 07:00:00', 'Setor Tunai', 'C', 50000);
INSERT INTO `transactions` VALUES (18, 3, '2017-03-07 07:00:00', 'Bayar Listrik', 'D', 125000);
INSERT INTO `transactions` VALUES (19, 3, '2017-03-15 07:00:00', 'Beli Pulsa', 'D', 20000);

SET FOREIGN_KEY_CHECKS = 1;
