package routes

import (
    "testGin/controllers"

    "github.com/gin-gonic/gin"
    "github.com/jinzhu/gorm"
)

func SetupRoutes(db *gorm.DB) *gin.Engine {
    r := gin.Default()
    r.Use(func(c *gin.Context) {
        c.Set("db", db)
    })
    r.GET("/customer/point", controllers.TotalPoint)
    r.POST("/transaction", controllers.CreateTransaction)
    r.POST("/pembukuan", controllers.Pembukuan)
	r.Run(":8089")
    return r
}