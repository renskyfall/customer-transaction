package main

import (
        // book adalah directory root project go yang kita buat
    "testGin/setup" // memanggil package models pada directory models
	"testGin/models"
    "testGin/routes"
)

func main() {

    db := setup.SetupDB()
    db.AutoMigrate(&models.Transaction{})
    db.AutoMigrate(&models.Customer{})
	
    r := routes.SetupRoutes(db)
    r.Run()
}